#! /usr/bin/env python3

# how to run:
#    pytest-3 matrixmult.py


import pytest
import numpy as np

def multiply(a, b):
    rows_a = len(a)
    columns_a = len(a[0])
    rows_b = len(b)
    columns_a = len(b[0])

    if columns_a != rows_b:
        print("Number of columns from matrix a is not equal to number of rows from matrix b")
        raise Exception
    else:
        m = [[0 for row in range(len(b[0]))] for col in range(len(a))]
        print(m)

    for i in range(rows_a):
        for j in range(columns_a):
            for k in range(rows_a):
                m[i][j] += a[i][k] * b[k][j]
    return m

    pass


def multiplyWithNumpy(a, b):
    return np.matmul(a, b)
    pass


testdata = [
    ([[3]], [[6]], [[18]]),
    ([[1, 2]],   [[3], [4]], [[11]]),
    ([[1, 2, 3], [4, 5, -7]],  [[3, 0, 4], [2, 5, 1], [-1, -1, 0]],  [[4, 7, 6], [29, 32, 21]])
]
@pytest.mark.parametrize("multiplicationFunc", [multiply, multiplyWithNumpy])
@pytest.mark.parametrize("a,b,expected", testdata)
def test_multiply(multiplicationFunc, a, b, expected):
    assert np.array_equal(multiplicationFunc(a, b), expected)


testdataImpossibleProducts = [
    ([[3, 4]], [[6, 7]]),
    ([[1, 2]], [[3, 4], [4, 5], [5, 6]])
]
@pytest.mark.parametrize("multiplicationFunc", [multiply, multiplyWithNumpy])
@pytest.mark.parametrize("a,b", testdataImpossibleProducts)
def test_multiplyIncompatibleMatrices(multiplicationFunc, a, b):
    with pytest.raises(Exception) as e:
        multiplicationFunc(a, b)



